import { expect, server, BASE_URL } from './setup';

const createUser = () => ({
  email: `test_${Date.now()}@test.me`,
  password: 'password'
});

const newUser = createUser();

describe('POST /api/v1/user test', () => {
  it('register user', done => {
    server
      .post(`${BASE_URL}/user`)
      .send(newUser)
      .expect(200)
      .end((err, res) => {
        expect(res.status).to.equal(200);
        expect(res.body.success).to.equal(1);
        expect(res.body.data.email).to.equal(newUser.email);
        done();
      });
  });

  it('get 409 error when trying to use registered email', done => {
    server
      .post(`${BASE_URL}/user`)
      .send(newUser)
      .expect(200)
      .end((err, res) => {
        expect(res.status).to.equal(409);
        expect(res.body.success).to.equal(0);
        expect(res.body.data.message).to.equal('Email address already in use');
        done();
      });
  });

  it('get 409 error when trying to use registered email', done => {
    server
      .post(`${BASE_URL}/user`)
      .send(newUser)
      .expect(200)
      .end((err, res) => {
        expect(res.status).to.equal(409);
        expect(res.body.success).to.equal(0);
        expect(res.body.data.message).to.equal('Email address already in use');
        done();
      });
  });

  it('get 400 "Validation error: Email is required"', done => {
    server
      .post(`${BASE_URL}/user`)
      .send({ password: 'password' })
      .expect(200)
      .end((err, res) => {
        expect(res.status).to.equal(400);
        expect(res.body.success).to.equal(0);
        expect(res.body.data.message).to.equal('Validation error');
        expect(res.body.data.errors)
          .to.be.an('array')
          .deep.include({
            msg: 'Email is required',
            param: 'email',
            location: 'body'
          });
        done();
      });
  });

  it('get 400 "Validation error: Incorrect email"', done => {
    server
      .post(`${BASE_URL}/user`)
      .send({ email: 'wrong@email', password: 'password' })
      .expect(200)
      .end((err, res) => {
        expect(res.status).to.equal(400);
        expect(res.body.success).to.equal(0);
        expect(res.body.data.message).to.equal('Validation error');
        expect(res.body.data.errors)
          .to.be.an('array')
          .deep.include({
            value: 'wrong@email',
            msg: 'Incorrect email',
            param: 'email',
            location: 'body'
          });
        done();
      });
  });

  it('get 400 "Validation error: Password is required"', done => {
    server
      .post(`${BASE_URL}/user`)
      .send({ email: createUser().email })
      .expect(200)
      .end((err, res) => {
        expect(res.status).to.equal(400);
        expect(res.body.success).to.equal(0);
        expect(res.body.data.message).to.equal('Validation error');
        expect(res.body.data.errors)
          .to.be.an('array')
          .deep.include({
            msg: 'Password is required',
            param: 'password',
            location: 'body'
          });
        done();
      });
  });

  it('get 400 "Validation error: Incorrect password (must be a string from 8 to 255 symbols length)"', done => {
    const user = createUser();
    user.password = 'pwd';

    server
      .post(`${BASE_URL}/user`)
      .send(user)
      .expect(200)
      .end((err, res) => {
        expect(res.status).to.equal(400);
        expect(res.body.success).to.equal(0);
        expect(res.body.data.message).to.equal('Validation error');
        expect(res.body.data.errors)
          .to.be.an('array')
          .deep.include({
            value: user.password,
            msg: 'Incorrect password (must be a string from 8 to 255 symbols length)',
            param: 'password',
            location: 'body'
          });
        done();
      });
  });
});
