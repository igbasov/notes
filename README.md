# Notes Service

## Installation

### 1. Node.js

1. Install `Node.js` and `npm` (using [nvm]https://github.com/nvm-sh/nvm))

```bash
# install nvm
wget -qO- https://raw.githubusercontent.com/nvm-sh/nvm/v0.37.0/install.sh | bash
# then restart your bash (or reload config)
# verify nvm installation
command -v nvm
# install Node.js 12
npm install 12
# verify Node.js installation
node --version
```

2. Install dependencies

```bash
npm install
```

### 2. PostgreSQL configuration

1. Create role (for example, with name `notesadmin` and password `notesadminpassword`)
2. Create database (for example, `notes`)
3. Create database for automatic testing (for example, `notes-auto-test`)

### 3. Env variables

1. Create file `.env` in the project root directory. Example:

```bash
NOTES_API_REQUEST_LOGGER_ENABLED=1
NOTES_API_AUT_TOKEN_LIFETIME=86400000
NOTES_API_MAX_PAGE_SIZE=30
NOTES_SERVER_PORT=3000
NOTES_DAO_PG_CONNECTION_STRING="postgres://notesadmin:notesadminpassword@localhost:5432/notes"
NOTES_DAO_PG_REQUEST_LOGGING=0
```

**NOTE:** if `NODE_ENV` env variable is not empty or not equal to `production` change file name (above) to `.env.<node_env>`, where `<node_env>` is equal to lowercased `NODE_ENV` value (for example, `NODE_ENV=development` --> `.env.development`)

2. Create file `.env.autotest` in the project root directory. Example:

```bash
NOTES_API_REQUEST_LOGGER_ENABLED=1
NOTES_API_AUT_TOKEN_LIFETIME=86400000
NOTES_API_MAX_PAGE_SIZE=30
NOTES_SERVER_PORT=3000
NOTES_DAO_PG_CONNECTION_STRING="postgres://notesadmin:notesadminpassword@localhost:5432/notes-auto-test"
NOTES_DAO_PG_REQUEST_LOGGING=0
```

### 4. Sync database

```bash
npm run migrate:up
```

It will create project's database structure and add test user. Test user data stores in `src/settings/data/migration-get-users.js` file.

### 5. Run

```bash
# start app
npm start
# start app in development mode (with watch enabled)
npm run dev:start
```

## Description

### API

**/api/v1/user**

1. `POST /api/v1/user body:{ email, password }` - create user. Returns:

```js
{ "success": 1, "data": { "email": "<new user email>" } }
```


**/api/v1/auth**

1. `POST /api/v1/auth/login { body: { email, password } }` - login. Returns:

```js
{
    "success": 1,
    "data": {
        "uuid": "d4f9026c-6f8d-4b7f-a50a-3c80999f55f1",
        "token": "5e3ddd25-2324-4a3c-a077-eca6a539fde1",
        "tokenExpiredAt": "1606235241676"
    }
}
```

2. `GET [AUTH]* /api/v1/auth/logout` - logout. Returns:

```js
{ "success": 1 }
```

[*] - you can provide token via: query (`?token=<uuid_token>`), `Authorize: "Bearer <token>"` HTTP header, cookie `token`

**/api/v1/note**

1. `GET [AUTH] /api/v1/note` - get all notes of authorized user. Authorization needed. Returns:

```js
{
    "success": 1,
    "data": [{
        "uuid": "58ac1938-ea4e-4ece-aa04-c0bedaf9a444",
        "text": "my first note",
        ...
    }, ...]
}
```

2. `GET [AUTH] /api/v1/note/:id` - get note by `id` of authorized user. Authorization needed. Returns:

```js
{
    "success": 1,
    "data": {
        "uuid": "58ac1938-ea4e-4ece-aa04-c0bedaf9a444",
        "text": "my first note",
        "readUUID": null,
        "createdAt": "2020-11-23T16:33:55.836Z",
        "updatedAt": "2020-11-23T16:33:55.836Z",
        "deletedAt": null,
        "authorId": "d4f9026c-6f8d-4b7f-a50a-3c80999f55f1"
    }
}
```

3. `POST [AUTH] /api/v1/note { body: { text } }` - create note with specified `text`. Returns:

```js
{
    "success": 1,
    "data": {
        "uuid": "58ac1938-ea4e-4ece-aa04-c0bedaf9a444",
        "authorId": "d4f9026c-6f8d-4b7f-a50a-3c80999f55f1",
        "text": "my first note",
        "updatedAt": "2020-11-23T16:33:55.836Z",
        "createdAt": "2020-11-23T16:33:55.836Z",
        "readUUID": null,
        "deletedAt": null
    }
}
```

4. `PUT [AUTH] /api/v1/note/:id { body: { text } }` - update note by `id` with specified `text`. Returns: note with changed `text` and `updatedAt` fields

5. `DELETE [AUTH] /api/v1/note/:id { body: { text } }` - delete note by `id`. Returns:

```js

```

6. `POST [AUTH] /api/v1/note/:id/share` - share note by `id`. Returns:

```js
{
    "success": 1,
    "data": {
        "uuid": "58ac1938-ea4e-4ece-aa04-c0bedaf9a444",
        "text": "my first note",
        "readUUID": "1669854f-142d-40e0-b6b3-6bfdd04fcd51",
        "createdAt": "2020-11-23T16:33:55.836Z",
        "updatedAt": "2020-11-23T16:37:10.085Z",
        "deletedAt": null,
        "authorId": "d4f9026c-6f8d-4b7f-a50a-3c80999f55f1"
    }
}
```

`readUUID` - public note identifier (`publicId`)

7. `GET /api/v1/note/public/:publicId` - get public note by `publicId`. Returns: requested note (if it exists and shared).




### Validation errors

Validation error examples:

1. `POST http://localhost:3000/api/v1/note/<incorrect UUID note identifier>/share?token=ad14a757-4db7-4d9a-93ad-fc2705029b82`
```js
{
    "success": 0,
    "data": {
        "message": "Validation error",
        "errors": [
            {
                "value": "58ac1938-ea4e-4ece-aa04-c0bedaf9a44",
                "msg": "id must be a UUIDv4 identifier",
                "param": "id",
                "location": "params"
            }
        ]
    }
}
```

2. `POST http://localhost:3000/api/v1/user { body: { email: <incorrect email>, password: 'password' } }`

```js
{
    "success": 0,
    "data": {
        "message": "Validation error",
        "errors": [
            {
                "value": "test@test",
                "msg": "Incorrect email",
                "param": "email",
                "location": "body"
            }
        ]
    }
}
```

3. Etc....

# Migration

[Migrate: docs](https://www.npmjs.com/package/migrate)

1. Create migration:

```bash
migrate create <name>
```

it will create new file in `migrations/` folder.

2. Create and use `up` and `down` handlers in folder `src/tools/migrations`. 

Use `src/tools/migrations/create-users.js` and `migrations/1605959012619-create-users.js` as a reference.

# Automatic testing and code coverage

1. Add data for `NODE_ENV=autotest` into `src/settings/data/` and ``src/settings/index.js` if needed
2. Run:

```bash
npm run test
```

3. Test will generate three code coverage reports:
    - console report
    - .html document in `/coverage` directory (see, `coverage/index.html`)
    - `lcov`-report in `.nyc_output/` directory

# Environment variables

- **NOTES_API_REQUEST_LOGGER_ENABLED** - enables API requests logging. Default is `1`. Set to `0` or remove var to disable logging.
- **NOTES_API_AUT_TOKEN_LIFETIME** - user authorization token lifetime (ms). Default is `86400000` (24 hours).
- **NOTES_API_MAX_PAGE_SIZE** - notes list page max size. Default is `30`.
- **NOTES_SERVER_PORT** - server port. Default is `3000`.
- **NOTES_DAO_PG_CONNECTION_STRING** - postgresql database connection string. Default is `postgres://notesadmin:notesadminpassword@localhost:5432/notes`.
- **NOTES_DAO_PG_REQUEST_LOGGING** - sequelize sql queries logging. Default is `0` - disabled. Set `1` to enable.

# Links

1. [Package: validator.js](https://github.com/validatorjs/validator.js)
2. [Package: express-validator](https://express-validator.github.io/docs/index.html)
3. [Package: http-errors](https://www.npmjs.com/package/http-errors)
4. [Package: sequelize](https://sequelize.org/)

# TODO

1. Create logging decorator for request handlers.
2. Create tests for `/api/v1/auth` and `/api/v1/note` routes.
3. Improve docs (add swagger file, maybe).
4. Improve transpilation mechanism (project building, debugging).
5. Containerize application.
6. Improve migration mechanism.
7. Improve code readability :)