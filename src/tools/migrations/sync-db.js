import debug from 'debug';

import dao from '../../dao';

const log = debug('MIGRATE:SYNC-DB:log');
const errorLog = debug('MIGRATE:SYNC-DB:error');

const { sync } = dao;

export const up = () => sync()
  .then(() => log('up: done'))
  .catch((error) => {
    errorLog('up: error', error);
    throw error;
  });

export const down = () => Promise.resolve(log('down: done'));
