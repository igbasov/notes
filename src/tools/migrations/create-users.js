import debug from 'debug';

import dao from '../../dao';
import { migrationGetUsers } from '../../settings';

const log = debug('MIGRATE:CREATE-USERS:log');
const errorLog = debug('MIGRATE:CREATE-USERS:error');

const { models, Sequelize } = dao;
const { User } = models;

const users = migrationGetUsers() || [];

export const up = () => User
  .bulkCreate(users, { individualHooks: true })
  .then(() => log('up: done'))
  .catch((error) => {
    errorLog('up: error', error);
    throw error;
  });

export const down = () => {
  const emails = users.map(u => u.email);
  return User
    .destroy({ where: { email: { [Sequelize.Op.in]: emails } }, force: true })
    .then(() => log('down: done'))
    .catch((error) => {
      errorLog('down: error', error);
      throw error;
    });
};
