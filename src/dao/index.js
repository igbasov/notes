import Sequelize from 'sequelize';
import path from 'path';
import glob from 'glob';
import debug from 'debug';

import {
  daoPGConnectionString,
  daoPGRequestLogging
} from '../settings';

const log = debug('DAO:log');

const dao = {
  Sequelize,
  models: {}
};

dao.db = new Sequelize(daoPGConnectionString, { logging: daoPGRequestLogging ? log : false });
dao.sync = () => dao.db.sync({ force: true });

/**
 * Get models
 */
glob
  .sync(`${path.join(__dirname, 'models')}/*.js`)
  .forEach(
    (pathToModel) => require(pathToModel.toString())(dao) // eslint-disable-line
  );

/**
 * Associate models
 */
Object
  .keys(dao.models)
  .forEach((modelName) => {
    if ('associate' in dao.models[modelName]) {
      dao.models[modelName].associate(dao.models);
    }
  });

log('initialized');

export default dao;
