// import debug from 'debug';

import { apiAuthTokenLifetime } from '../../settings';

// const log = debug('DAO:AUTHTOKEN:log');
// const errorLog = debug('DAO:AUTHTOKEN:error');

module.exports = ({ Sequelize, models, db }) => {
  const DataTypes = Sequelize;
  const schema = {
    uuid: {
      primaryKey: true,
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4
    },
    expiredAt: {
      primaryKey: true,
      type: DataTypes.BIGINT,
      allowNull: false,
      defaultValue: () => Date.now() + apiAuthTokenLifetime
    }
  };
  const options = {
    sequelize: db,
    freezeTableName: true,
    tableName: 'AuthToken',
    paranoid: true
  };

  class AuthToken extends Sequelize.Model {
    static associate({ User }) {
      AuthToken.belongsTo(User, { as: 'user', foreignKey: 'userId' });
    }
  }

  AuthToken.init(schema, options);

  models.AuthToken = AuthToken;
};
