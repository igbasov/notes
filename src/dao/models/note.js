import { v4 as uuidv4 } from 'uuid';
// import debug from 'debug';

// const log = debug('DAO:USER:log');
// const errorLog = debug('DAO:USER:error');

module.exports = ({ Sequelize, models, db }) => {
  const DataTypes = Sequelize;
  const schema = {
    uuid: {
      primaryKey: true,
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4
    },
    text: {
      type: DataTypes.STRING(1000),
      defaultValue: ''
    },
    readUUID: {
      type: DataTypes.UUID,
      allowNull: true
    }
  };
  const options = {
    sequelize: db,
    freezeTableName: true,
    tableName: 'Note',
    paranoid: true,
    indexes: [
      { fields: ['readUUID'] },
    ]
  };

  class Note extends Sequelize.Model {
    static associate({ User }) {
      Note.belongsTo(User, { as: 'author', foreignKey: 'authorId' });
    }

    static share(uuid, authorId) {
      return Note.update(
        { readUUID: uuidv4() },
        {
          where: { authorId, uuid },
          returning: true
        }
      );
    }
  }

  Note.init(schema, options);

  models.Note = Note;
};
