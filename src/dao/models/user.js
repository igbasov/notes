import bcrypt from 'bcryptjs';
import debug from 'debug';
import createError from 'http-errors';

// const log = debug('DAO:USER:log');
const errorLog = debug('DAO:USER:error');

module.exports = ({ Sequelize, models, db }) => {
  const DataTypes = Sequelize;
  const schema = {
    uuid: {
      primaryKey: true,
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4
    },
    email: {
      type: DataTypes.STRING(200),
      notNull: true,
      unique: true,
      validate: {
        isEmail: true
      }
    },
    password: {
      type: DataTypes.STRING,
      notNull: true,
      validate: {
        len: {
          args: [8, 255],
          msg: 'Password must be from 8 to 255 characters long',
        },
      },
    }
  };
  const options = {
    sequelize: db,
    freezeTableName: true,
    tableName: 'User',
    paranoid: true,
    indexes: [
      { fields: ['email'], unique: true },
    ]
  };

  const createAuthUserDTO = (user, token) => ({
    uuid: user.uuid,
    token: token.uuid,
    tokenExpiredAt: token.expiredAt
  });

  class User extends Sequelize.Model {
    static associate({ AuthToken, Note }) {
      User.hasMany(AuthToken, { as: 'authTokens', foreignKey: 'userId' });
      User.hasMany(Note, { as: 'notes', foreignKey: 'authorId' });
    }

    static async authByCredentials(email, password) {
      try {
        const user = await User.findOne({ where: { email } });

        if (!user) {
          throw createError(401, 'Wrong email or/and password');
        }

        if (user.checkPassword(password)) {
          return user
            .createAuthToken({ userId: user.id })
            .then((token) => createAuthUserDTO(user, token));
        }

        return false;
      } catch (error) {
        errorLog(`authByCredentials: email "${email}"`, error);
        throw error;
      }
    }

    static async authByToken(token) {
      try {
        const user = await User.findByToken(token);
        if (!user) {
          throw createError(401, 'Unauthorized');
        }

        return createAuthUserDTO(user, user.authTokens[0]);
      } catch (error) {
        errorLog(`authByToken: token "${token}"`, error);
        throw error;
      }
    }

    static async logout(token) {
      try {
        const user = await User.findByToken(token);
        if (!user) {
          throw createError(401, 'Unauthorized');
        }

        return models
          .AuthToken
          .destroy({ where: { userId: user.uuid } });
      } catch (error) {
        errorLog(`logout: token "${token}"`, error);
        throw error;
      }
    }

    static async findByToken(token) {
      try {
        return User.findOne({
          include: {
            model: models.AuthToken,
            as: 'authTokens',
            where: {
              uuid: token,
              expiredAt: { [Sequelize.Op.gte]: Date.now() }
            }
          }
        });
      } catch (error) {
        errorLog(`authByToken: token "${token}"`, error);
        throw error;
      }
    }

    async checkPassword(passwordToCheck) {
      try {
        return await bcrypt.compare(passwordToCheck, this.password);
      } catch (error) {
        errorLog('checkPassword', error);
        throw error;
      }
    }
  }

  User.init(schema, options);

  // eslint-disable-next-line no-shadow
  User.addHook('beforeCreate', async (user, options, next) => {
    try {
      const salt = await bcrypt.genSalt(10);
      const passwordHash = await bcrypt.hash(user.password, salt);
      user.password = passwordHash;
    } catch (error) {
      errorLog(error);
      next(error);
    }
  });

  models.User = User;
};
