import { Router } from 'express';

import authMiddleware from '../../../middlewares/auth';
import {
  getNoteValidator,
  getNotesValidator,
  createNoteValidator,
  updateNoteValidator,
  deleteNoteValidator,
  shareNoteValidator,
  getPublicNoteValidator
} from './note.validators';
import {
  getNote,
  getNotes,
  createNote,
  updateNote,
  deleteNote,
  shareNote,
  getPublicNote
} from './note.controllers';

const router = Router();

router.get('/public/:publicId', getPublicNoteValidator, getPublicNote);
router.get('/:id', authMiddleware, getNoteValidator, getNote);
router.get('/', authMiddleware, getNotesValidator, getNotes);
router.post('/', authMiddleware, createNoteValidator, createNote);
router.put('/:id', authMiddleware, updateNoteValidator, updateNote);
router.delete('/:id', authMiddleware, deleteNoteValidator, deleteNote);
router.post('/:id/share', authMiddleware, shareNoteValidator, shareNote);

export default router;
