import { check } from 'express-validator';

import createValidator from '../../../middlewares/validator-middleware-factory';

const checkTextField = check('text')
  .isString()
  .withMessage('text is required')
  .bail()
  .isLength({ min: 0, max: 1000 })
  .withMessage('text have to be a string up to 1000 symbols length');

const checkIdParameter = check('id')
  .isString()
  .withMessage('id is required')
  .bail()
  .isUUID(4)
  .withMessage('id must be a UUIDv4 identifier');

const checkPublicIdParameter = check('publicId')
  .isString()
  .withMessage('publicId is required')
  .bail()
  .isUUID(4)
  .withMessage('id must be a UUIDv4 identifier');

export const noteValidator = createValidator([
  checkTextField
]);

export const getNoteValidator = createValidator([
  checkIdParameter
]);

export const getNotesValidator = createValidator([
  check('page')
    .optional()
    .isInt()
    .withMessage('page must be an integer')
    .bail(),
  check('limit')
    .optional()
    .isInt()
    .withMessage('limit must be an integer')
    .bail(),
]);

export const createNoteValidator = createValidator([
  checkTextField
]);

export const updateNoteValidator = createValidator([
  checkIdParameter,
  checkTextField
]);

export const deleteNoteValidator = createValidator([
  checkIdParameter
]);

export const shareNoteValidator = createValidator([
  checkIdParameter
]);

export const getPublicNoteValidator = createValidator([
  checkPublicIdParameter
]);
