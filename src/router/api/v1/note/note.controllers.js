import createError from 'http-errors';
import { v4 as uuidv4 } from 'uuid';

import dao from '../../../../dao';
import { apiMaxPageSize } from '../../../../settings';

const { Note } = dao.models;

export async function getNote(req, res, next) {
  try {
    req.logger.log('received');

    const note = await Note.findOne({
      where: {
        authorId: req.user.uuid,
        uuid: req.params.id
      }
    });

    if (!note) {
      throw createError(404, 'Note not found');
    }

    res.json({ success: 1, data: note });
    req.logger.log('success');
  } catch (error) {
    next(error);
  }
}

export async function getNotes(req, res, next) {
  try {
    req.logger.log('received');

    const { page = 1, limit: pageSize = 10 } = req.query;
    const limit = +pageSize > apiMaxPageSize ? apiMaxPageSize : +pageSize;
    const offset = (page - 1) * limit;

    const notes = await Note.findAll({
      where: { authorId: req.user.uuid },
      limit,
      offset
    });

    res.json({ success: 1, data: notes });
    req.logger.log('success');
  } catch (error) {
    next(error);
  }
}

export async function createNote(req, res, next) {
  try {
    req.logger.log('received');

    const note = await Note.create({
      authorId: req.user.uuid,
      text: req.body.text
    });

    res.json({ success: 1, data: note });
    req.logger.log('success');
  } catch (error) {
    next(error);
  }
}

export async function updateNote(req, res, next) {
  try {
    req.logger.log('received');

    const result = await Note.update(
      { text: req.body.text },
      {
        where: {
          authorId: req.user.uuid,
          uuid: req.params.id
        },
        returning: true
      }
    );

    if (!result[1].length) {
      throw createError(404, 'Not found');
    }

    res.json({ success: 1, data: result[1][0] });
    req.logger.log('success');
  } catch (error) {
    next(error);
  }
}

export async function deleteNote(req, res, next) {
  try {
    req.logger.log('received');

    const result = await Note.destroy({
      where: {
        authorId: req.user.uuid,
        uuid: req.params.id
      }
    });

    res.json({ success: 1, data: result === 1 });
    req.logger.log('success');
  } catch (error) {
    next(error);
  }
}

export async function shareNote(req, res, next) {
  try {
    req.logger.log('received');

    const result = await Note.share(req.params.id, req.user.uuid);

    if (!result[1].length) {
      throw createError(404, 'Not found');
    }

    res.json({ success: 1, data: result[1][0] });
    req.logger.log('success');
  } catch (error) {
    next(error);
  }
}

export async function getPublicNote(req, res, next) {
  try {
    req.logger.log('received');

    const note = await Note.findOne({
      where: { readUUID: req.params.publicId }
    });

    if (!note) {
      throw createError(404, 'Note not found');
    }

    res.json({ success: 1, data: note });
    req.logger.log('success');
  } catch (error) {
    next(error);
  }
}
