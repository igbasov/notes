import express from 'express';

import addLogger from '../../middlewares/add-logger';

import authRouter from './auth';
import noteRouter from './note';
import userRouter from './user';

const router = express.Router({ mergeParams: true });

router.use('/auth', addLogger('API:AUTH'), authRouter);
router.use('/note', addLogger('API:NOTE'), noteRouter);
router.use('/user', addLogger('API:USER'), userRouter);

export default router;
