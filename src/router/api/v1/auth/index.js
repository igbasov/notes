import { Router } from 'express';

import { login, logout } from './auth.controllers';
import { loginValidator } from './auth.validators';
import authMiddleware from '../../../middlewares/auth';

const router = Router();

router.post('/login', loginValidator, login);
router.get('/logout', authMiddleware, logout);

export default router;
