import dao from '../../../../dao';

const { User } = dao.models;

export const login = async (req, res, next) => {
  try {
    req.logger.log('received');
    const { email, password } = req.body;
    const data = await User.authByCredentials(email, password);

    res.json({ success: 1, data });
    req.logger.log('success');
  } catch (error) {
    next(error);
  }
};

export const logout = async (req, res, next) => {
  try {
    await User.logout(req.user.token);

    res.json({ success: 1 });
    next();
  } catch (error) {
    next(error);
  }
};
