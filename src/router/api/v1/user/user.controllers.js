import createError from 'http-errors';

import dao from '../../../../dao';

const { User } = dao.models;

export async function signup(req, res, next) {
  try {
    req.logger.log('received');

    const user = await User.create(req.body);

    res.json({
      success: 1,
      data: { email: user.toJSON().email }
    });
    req.logger.log('success');
  } catch (error) {
    if (error.name === 'SequelizeUniqueConstraintError') {
      next(createError(409, 'Email address already in use'));
    } else {
      next(error);
    }
  }
}
