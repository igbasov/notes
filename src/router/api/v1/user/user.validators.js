import { check } from 'express-validator';

import createValidator from '../../../middlewares/validator-middleware-factory';

export const signupValidator = createValidator([
  check('email')
    .isString()
    .withMessage('Email is required')
    .bail()
    .normalizeEmail()
    .isEmail()
    .withMessage('Incorrect email'),
  check('password')
    .isString()
    .withMessage('Password is required')
    .bail()
    .trim()
    .isLength({ min: 8, max: 255 })
    .withMessage('Incorrect password (must be a string from 8 to 255 symbols length)')
]);
