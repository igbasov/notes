import { Router } from 'express';

import { signup } from './user.controllers';
import { signupValidator } from './user.validators';

const router = Router();

router.post('/', signupValidator, signup);

export default router;
