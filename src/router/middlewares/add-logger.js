import { v4 as uuidv4 } from 'uuid';
import debug from 'debug';

import { apiRequestLoggerEnabled } from '../../settings';

export default (label) => (req, res, next) => {
  if (apiRequestLoggerEnabled) {
    req.id = uuidv4();
    const log = debug(`${label}:${req.id}`);

    req.logger = {
      log(...args) {
        log(`[INF] ${req.method.toUpperCase()} ${req.originalUrl}`, ...args);
      },
      error(...args) {
        log(`[ERR] ${req.method.toUpperCase()} ${req.originalUrl}`, ...args);
      }
    };

    next();
  } else {
    req.logger = { log() { }, error() { } };
    next();
  }
};
