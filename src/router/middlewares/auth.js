import passport from 'passport';
import createError from 'http-errors';

module.exports = (req, res, next) => {
  const token = (req.headers.authorization || '').split(' ')[1]
    || req.cookies.token
    || req.query.token;

  if (!token) {
    next(createError(401, 'Please provide an authentication token'));
  }

  req.headers.authorization = `Bearer ${token}`;
  passport
    .authenticate('bearer', { session: false }, (err, user) => {
      if (err || !user) {
        next(createError(401, 'Unauthorized'));
      } else {
        req.user = user;
        next();
      }
    })(req, res, next);
};
