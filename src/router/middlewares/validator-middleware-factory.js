import { validationResult } from 'express-validator';
import createError from 'http-errors';

export default (checksArray) => [
  ...checksArray,
  (req, res, next) => {
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
      const error = createError(400, 'Validation error');
      error.validationErrors = errors.array();
      next(error);
    }

    next();
  }
];
