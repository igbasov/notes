export default (error, req, res, next) => { // eslint-disable-line no-unused-vars
  req.logger.error(error);
  if (!res.finished) {
    res
      .status(error.status || 500)
      .json({
        success: 0,
        data: {
          message: error.status ? error.message : 'Server error',
          errors: error.validationErrors || undefined
        }
      });
  }
};
