export default (req, res) => {
  res
    .status(404)
    .json({ success: 0, data: { message: 'Not found' } });
};
