import express from 'express';

import notFoundMiddleware from './middlewares/not-found-error-handler';
import commonErrorMiddleware from './middlewares/common-error-handler';
import apiV1Router from './api/v1/index';

const router = express.Router();

router.use('/api/v1', apiV1Router);
router.use(notFoundMiddleware);
router.use(commonErrorMiddleware);

export default router;
