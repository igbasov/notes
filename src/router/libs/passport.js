import passport from 'passport';
import BearerStrategy from 'passport-http-bearer';

import dao from '../../dao';

passport.use(new BearerStrategy(
  async (token, done) => {
    try {
      const user = await dao.models.User.authByToken(token);
      done(null, user || false);
    } catch (error) {
      done(error);
    }
  }
));
