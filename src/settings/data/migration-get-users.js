const data = {
  development: [
    { email: 'basovigorg@gmail.com', password: 'password' }
  ],
  test: [],
  production: []
};

export const getUsers = () => {
  const env = process.env.NODE_ENV || 'development';
  return data[env];
};
