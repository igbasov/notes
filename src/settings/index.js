import dotenv from 'dotenv';
import debug from 'debug';
import path from 'path';

import { getUsers } from './data/migration-get-users';

const errorLog = debug('API:config:error');
const configFileExt = !process.env.NODE_ENV || process.env.NODE_ENV === 'production'
  ? ''
  : `.${process.env.NODE_ENV.toLowerCase()}`;

const configResult = dotenv.config({ path: path.resolve(process.cwd(), `.env${configFileExt}`) });

if (configResult.error) {
  errorLog(configResult.error.message);
  throw configResult.error;
}

// Server constants
export const serverPort = process.env.NOTES_SERVER_PORT;

// API constatns
export const apiRequestLoggerEnabled = process.env.NOTES_API_REQUEST_LOGGER_ENABLED || false;
export const apiAuthTokenLifetime = +process.env.NOTES_API_AUT_TOKEN_LIFETIME || 86400000;
export const apiMaxPageSize = +process.env.NOTES_API_MAX_PAGE_SIZE || 30;

// DAO constants
export const daoPGConnectionString = process.env.NOTES_DAO_PG_CONNECTION_STRING || 'postgres://notesadmin:notesadminpassword@localhost:5432/notes';
export const daoPGRequestLogging = +process.env.NOTES_DAO_PG_REQUEST_LOGGING || 0;

// Migration
export const migrationGetUsers = getUsers;
