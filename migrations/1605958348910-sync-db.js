const { up, down } = require('../build/tools/migrations/sync-db');

module.exports.up = up;
module.exports.down = down;
